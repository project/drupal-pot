Because the Drupal interface in the current development version is
still target of changes, we are not generating new translation templates
until the interface stabilizes, and even then, we add the templates to
one of the branches of this project. Look into the various branches of
this project for stable translation templates.

